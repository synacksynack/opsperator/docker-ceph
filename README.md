# DEPRECATED

RadosGW moved to:
https://gitlab.com/synacksynack/opsperator/docker-radosgw

Other Ceph components support discontinued.
For a complete CNS implementation, better look into Rook.

# k8s Ceph

Generic Ceph image running s3 Gateways on Kubernetes.

Forked from https://github.com/ceph/ceph-container/

Build with:
```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

## Deploy RadosGW connecting to Arbitrary Cluster

Assuming the following Ceph configuration:

```sh
# grep -vE '^(#|[ ]*$)' /etc/ceph/ceph.conf
[global]
fsid = f980b615-746a-4e5e-b429-a364fd69838b
mon initial members = mon1,mon2,mon3
mon host = [v2:10.42.253.110:3300,v1:10.42.253.110:6789],[v2:10.42.253.111:3300,v1:10.42.253.111:6789],[v2:10.42.253.112:3300,v1:10.42.253.112:6789]
...
```

Create a keyring for RadosGW:

```sh
# ceph auth get-or-create client.radosgw.gateway osd 'allow rwx' mon 'allow rwx' -o /etc/ceph/ceph.client.radosgw.gateway.keyring
# cat /etc/ceph/ceph.client.radosgw.gateway.keyring
[client.radosgw.gateway]
        key = abcdefxxx==
```

Deploy RadosGW (if non-FQDN names involved: replace with IPs or FQDNs)

```sh
# oc process -f deploy/openshift/secret.yaml \
    -p CEPH_INITIAL_MEMBERS=mon1,mon2,mon3 \
    -p CEPH_CLUSTER_ID=f980b615-746a-4e5e-b429-a364fd69838b \
    -p CEPH_MON_HOSTS="[v2:10.42.253.110:3300,v1:10.42.253.110:6789],[v2:10.42.253.111:3300,v1:10.42.253.111:6789],[v2:10.42.253.112:3300,v1:10.42.253.112:6789]" \
    -p RADOSGW_KEYRING_KEY=abcdefxxx== \
    -p RADOSGW_KEYRING_NAME=radosgw.gateway | oc apply -f-
# oc process -f deploy/openshift/run-ephemeral.yaml \
    -p RADOSGW_KEYRING_NAME=radosgw.gateway | oc apply -f-
```

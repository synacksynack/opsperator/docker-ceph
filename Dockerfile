FROM ubuntu:20.04

# Ceph image for OpenShift Origin

ARG DO_UPGRADE=
ENV CEPH_VERSION=octopus \
    CONFD_VERSION=0.16.0 \
    KUBECTL_ROOTURL=https://dl.k8s.io \
    KUBECTL_VERSION=v1.20.2 \
    DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="Ceph is a distributed storage solution." \
      io.k8s.display-name="Ceph $CEPH_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="ceph,mon,osd,rgw,mgr,rbd-mirror,restapi-gw" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-ceph" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="15.2.5"

ADD config/s3cfg /root/.s3cfg
ADD config/*.sh config/ceph.defaults config/check_zombie_mons.py \
    config/osd_scenarios/* config/entrypoint.sh.in config/disabled_scenario /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools"; \
	apt-get -y install strace; \
    fi \
    && echo "# Install Ceph Dependencies" \
    && apt-get install -y wget unzip uuid-runtime python-setuptools udev \
	dmsetup libnss-wrapper gnupg \
    && echo "# Install Ceph" \
    && wget -q -O- https://download.ceph.com/keys/release.asc | apt-key add - \
    && echo deb http://download.ceph.com/debian-$CEPH_VERSION/ focal main \
	>/etc/apt/sources.list.d/ceph-$CEPH_VERSION.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends --force-yes ceph-mon \
	ceph-osd ceph-mds ceph-mgr ceph-base ceph-common radosgw rbd-mirror \
	sharutils etcd s3cmd lvm2 \
    && echo "# Intalling Dependencies" \
    && if test `uname -m` = aarch64; then \
	export PKGARCH=arm64; \
    else \
	export PKGARCH=amd64; \
    fi \
    && wget -O /usr/local/bin/confd \
	"https://github.com/kelseyhightower/confd/releases/download/v$CONFD_VERSION/confd-$CONFD_VERSION-linux-$PKGARCH" \
    && mkdir -p /etc/confd/conf.d /etc/confd/templates \
    && wget -O /forego.tgz \
	"https://bin.equinox.io/c/ekMN3bCZFUn/forego-stable-linux-$PKGARCH.tgz" \
    && tar -C /usr/local/bin -xzf /forego.tgz \
    && wget -O /kubernetes-client.tar.gz \
	$KUBECTL_ROOTURL/$KUBECTL_VERSION/kubernetes-client-linux-$PKGARCH.tar.gz \
    && tar -C /usr/local/bin -xzf /kubernetes-client.tar.gz \
	--strip-component 3 kubernetes/client/bin/kubectl \
    && echo "# Fixing permissions" \
    && chmod +x /usr/local/bin/kubectl /usr/local/bin/forego \
	/usr/local/bin/confd \
    && for dir in /etc/ceph /etc/ganesha /var/lib/ceph /var/run/ceph /var/run; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && ( chown -R 1001:root $dir || echo nevermind ) \
	    && ( chmod -R g=u $dir || echo nevermind ) ; \
	done \
    && ./generate_entrypoint.sh \
    && rm -f /forego.tgz /generate_entrypoint.sh /kubernetes-client.tar.gz \
    && bash -n /*.sh \
    && echo "# Cleaning Up" \
    && apt-get remove --purge -y unzip \
    && apt-get autoremove --purge -y \
    && apt-get clean \
    && rm -rvf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
	/usr/lib/{dracut,locale,systemd,udev} /usr/bin/hyperkube /usr/bin/etcd \
	/usr/bin/systemd-analyze /etc/{udev,selinux} /usr/lib/{udev,systemd} \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ADD config/confd/templates/* /etc/confd/templates/
ADD config/confd/conf.d/* /etc/confd/conf.d/

WORKDIR /
ENTRYPOINT [ "/run-ceph.sh" ]
USER 1001
